FROM registry.sindominio.net/debian as builder

# Support custom branches of the react-sdk and js-sdk. This also helps us build
# images of riot-web develop.
ARG USE_CUSTOM_SDKS=false
ARG REACT_SDK_REPO="https://github.com/matrix-org/matrix-react-sdk.git"
ARG REACT_SDK_BRANCH="master"
ARG JS_SDK_REPO="https://github.com/matrix-org/matrix-js-sdk.git"
ARG JS_SDK_BRANCH="master"
ARG NODE_OPTIONS="--openssl-legacy-provider"

# a hack to get nodejs >= 14, let's change it as soon as is in backports
RUN echo "deb http://deb.debian.org/debian sid main" >> /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y --no-install-recommends -t sid nodejs
RUN apt-get update && apt-get install -y git dos2unix yarnpkg gnupg jq curl ca-certificates
RUN git clone https://github.com/vector-im/element-web/ /src

WORKDIR /src

# Fix: Change key https://packages.riot.im/
COPY element-release-key.asc /element-release-key.asc

RUN TAG_NAME=`curl -s https://api.github.com/repos/vector-im/element-web/releases/latest |jq -r '.tag_name'` && \
    gpg --import /element-release-key.asc && \
    git verify-tag $TAG_NAME && \
    git checkout $TAG_NAME

RUN dos2unix /src/scripts/docker-link-repos.sh && bash /src/scripts/docker-link-repos.sh
RUN yarnpkg install

RUN dos2unix /src/scripts/docker-package.sh && \
    sed -i 's/yarn/yarnpkg/' /src/scripts/docker-package.sh && \
    bash /src/scripts/docker-package.sh

# App
FROM registry.sindominio.net/nginx

COPY --from=builder /src/webapp /app
COPY config.json /app/config.json

# set up nginx configuration
RUN sed -i '3i\ \ \ \ application/wasm wasm\;' /etc/nginx/mime.types

RUN rm -rf /var/www/html \
 && ln -s /app /var/www/html
